<?php

$conn = new PDO("mysql:host=localhost;dbname=test", "root", "");
// set the PDO error mode to exception
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);;

$db = getEventArchive($conn);

foreach($db as $row)
{
  echo $row['id'] . "  " . $row['title'] . " "
        . $row['date'] . $row['description'] . "<br>";
}

if(count($db) == 0) $db = null;

// test Constructor
construct($db, $conn);

function construct($db = null, PDO $conn)
{
  if ($db) {
      //$this->db = $db;
      $sql = "INSERT INTO testtable (title, date, description, id)
              VALUES ('Buddy Week Opening Concert', '2019-08-13',
                  'Featuring Kjartan Lauritzen', 4)";
      $conn->exec($sql);
      echo "Database connected successfully to sql";
  } else {
    try {

        $sql = array(
          "INSERT INTO testtable (title, date, description, id)
            VALUES
              ('Buddy Week Opening Concert', '2019-08-13',
                  'Featuring Kjartan Lauritzen', 1),
              ('Quiz Night', '2019-08-15',
                  'J.J. Confuser is quiz master', 2),
              ('Buddy Week Closing Concert', '2019-08-24',
                  'Featuring Kakkamadafakka', 3);"
        );

        foreach($sql as $A) $conn->exec($A);

        echo "Datbase created successfully</br>";
        }
    catch(PDOException $e)
        {
        echo $sql . "<br>" . $e->getMessage();
        }
  }
}

function getEventArchive(PDO $conn)
{
  $eventlist = $conn->query("SELECT id, title, date, description FROM testtable")->fetchAll();

  return $eventlist;
}

?>
