<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $conn, $nextID;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function __construct($db = null)
    {
        $this->conn = new PDO("mysql:host=localhost;dbname=test", "root", "");
        // set the PDO error mode to exception
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        try {

          $db = $this->getEventArchive();

          if ($db) {
            foreach($db as $row)
            {
              echo $row->id . "  " . $row->title . " "
                    . $row->date . " " . $row->description . "<br>";
            }
            $this->nextID = count($db) + 1;

            echo "Database fetched successfully from sql";
          } else {

            $sql = array(
              "INSERT INTO event (title, date, description, id)
                VALUES
                ('Buddy Week Opening Concert', '2019-08-13',
                    'Featuring Kjartan Lauritzen', 1),
                ('Quiz Night', '2019-08-15',
                    'J.J. Confuser is quiz master', 2),
                ('Buddy Week Closing Concert', '2019-08-24',
                    'Featuring Kakkamadafakka', 3);"
            );
            $this->nextID = 4;

            foreach($sql as $A) {
              $this->conn->exec($A);
            }

            echo "Datbase created successfully</br>";
            }
          }
        catch(PDOException $e)
            {
            echo  "Error message <br>" . $e->getMessage();
            }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */

    public function getEventArchive()
    {
      try {
        $event = null;
        $eventlist = $this->conn->query("SELECT
          id, title, date, description FROM event")->fetchAll();
        foreach ($eventlist as $row)
        {
            $event[$row['id']-1] = new Event(
              $row['title'],
              $row['date'],
              $row['description'],
              $row['id']
            );
        }
        return $event;
      }
      catch (PDOException $e){
        echo $event . "<br>" . $e->getMessage();
      }
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
      Event::verifyId($id);

      try{

        $eventlist = $this->getEventArchive();
        for($i = 0; $i < count($eventlist); $i++)
        {
          if($eventlist[$i]->id == $id)
          {
            return $eventlist[$i];
          }
        }
        return null;
      }
      catch(PDOException $e) {
        echo $eventlist . "<br>" . $e->getMessage();
      }
    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
        // TODO: Add the event to the database
        // Make sure event contains valid data
        $event->verify(true);
        try{
          // Insert event in archive

          $event->id = $this->nextID;
          $sql = "INSERT INTO event(title, date, description, id) VALUES (?, ?, ?, ?)";
          $stmt = $this->conn->prepare($sql);
          $stmt->execute([$event->title, $event->date, $event->description, $event->id]);
        }
        catch(PDOException $e){
          echo "Error message" . $e-getMessage();
        }
        catch(InvalidArgumentException $e)
        {
          echo "Invalid argument" . $e->getMessage();
        }
    }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {
        // TODO: Modify the event in the database
        // Make sure event contains valid data
        $event->verify();
        try{
          $sql = "UPDATE event SET title = ?, date = ?, description = ? WHERE id = ?";
          $stmt = $this->conn->prepare($sql);
          $stmt->execute([$event->title, $event->date, $event->description, $event->id]);
        }
        catch(PDOException $e){
          echo "Error message" . $e-getMessage();
        }
        catch(InvalidArgumentException $e)
        {
          echo "Invalid argument" . $e->getMessage();
        }
    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
        // TODO: Delete the event from the database
        Event::verifyId($id);
        try{
          $sql = "DELETE FROM event WHERE id = $id";
          $this->conn->exec($sql);

          for($i = $id; $i < $this->nextID; $i++)
          {
            $sql = "UPDATE event SET id = ? WHERE id = $i + 1";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute([$i]);
          }

          $this->nextID--;
        }
        catch(PDOException $e){
          echo "Error message" . $e-getMessage();
        }
    }
}
